
#define MAINSECTIONNAME			"Main"

#define VARACT_NONE			0
#define VARACT_COPY			1
#define VARACT_UNZIP			2
#define VARACT_UNARJ			3


typedef struct {
	int barpos;
	int liststart;
} VariousSectionSaveInfo;


void			variousManager(void);
void			variousCloseMainFile(void);
void			variousShutDown(void);
char			*variousCopySubStr(char *, char *);
char			*variousCopyNextSubStr(char *);
void			variousClearListArea(void);
void			variousBuildList(void);
void			variousPutListBar(int);
void			variousRemoveListBar(int);
char			*variousGetProgramInfo(int);
void			variousClearTags(void);
void			variousClearSaveInfo(void);
void			variousChangeSection(int, int, int);
