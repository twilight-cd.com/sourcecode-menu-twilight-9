{ TWiLiGHT Easy Installation V1.0 - (C) 1996 Twilight Crew }


{$M $8000,0,0}

uses dos, crt, subspro;


const
  popspeed = 4;
  itemscolor = lightcyan;

var
  xorcount, optystart, aantoptions, tel, tel2, tel3: integer;
  ch: char;

  xorstr: string;
  runstr, setupstr, docstr: string;
  configstr: array[1..3] of string;

  optstr: array[1..4] of string;

  viewbuf: array[1..25] of string[80];

  screenbuf: array[1..80*25*2] of byte;


procedure WaitVbl;
begin
  asm
         push    dx
         push    ax
         mov     dx,3dah
@wait:   in      al,dx
         test    al,8
         jz      @wait
@wait2:  in      al,dx
         test    al,8
         jnz     @wait2
         pop     ax
         pop     dx
  end;
end;


procedure Fatal(reason: string);
begin
  textmode(co80);
  textcolor(lightgray);
  textbackground(black);
  writeln('Fatal error: ', reason);
  halt(1);
end;


function UpCaseStr(str: string): string;
var
  tel1 : integer;
begin
  for tel1 := 1 to length (str) do str [tel1] := upcase (str [tel1]);
  UpCaseStr := str;
end;


procedure writeAt(x, y: integer; regel: string);
var
  attr, tel, adr: integer;
begin
  adr := (y - 1) * 80 * 2 + (x - 1) * 2;
  attr := textattr;

  for tel := 1 to length(regel) do begin
    mem[$b800:adr] := ord(regel[tel]);
    mem[$b800:adr+1] := attr;
    inc(adr, 2);
  end;
end;


procedure writeAt2(x, y, attr: integer; regel: string);
var
  tel, adr: integer;
begin
  adr := (y - 1) * 80 * 2 + (x - 1) * 2;

  for tel := 1 to length(regel) do begin
    mem[$b800:adr] := ord(regel[tel]);
    mem[$b800:adr+1] := attr;
    inc(adr, 2);
  end;
end;


procedure disableBlinking;
begin
  asm
    push ax
    push bx
    mov  ax,1003h
    xor  bl,bl
    int  10h
    pop  bx
    pop  ax
  end;
end;


procedure pause;
var
  ch: char;
begin
  textcolor(white);
  textbackground(blue);
  writeAt(1, 25, ' Hit any key to continue.                                                       ');
  while keypressed do ch := readkey;
  ch := readkey;
end;


procedure popupWindow(sx, sy, ex, ey: integer);
var
  curx, tel3, tel2, tel: integer;
  einde: boolean;
  leegstr: string;
begin
  leegstr := ''; for tel := 1 to popspeed do leegstr := leegstr + ' ';
  textbackground(blue);
  textcolor(white);

  tel := 2; einde := false;
  while not einde do begin
    waitVbl;
    curx := sx + (ex - sx + 1) div 2 - 1 - tel div 2;
    gotoxy(curx, sy);
    write('�'); for tel2 := 1 to tel - 2 do write('�'); write('�');

    for tel2 := sy + 1 to ey - 1 do begin
      gotoxy(curx, tel2);
      write('�');
      if tel >= popspeed then begin
        write(leegstr);
        gotoxy(curx + tel - 1 - popspeed, tel2);
        write(leegstr, '�');
      end else begin
        gotoxy(curx + tel, tel2);
        write('�');
      end;
      mem[$b800:wherey*160-160 + wherex*2-2+1] := mem[$b800:wherey*160-160+wherex*2-2+1] and 7;
      mem[$b800:wherey*160-160 + wherex*2+1] := mem[$b800:wherey*160-160+wherex*2+1] and 7;
    end;

    gotoxy(curx, ey);
    write('�'); for tel2 := 1 to tel - 2 do write('�'); write('�');
    mem[$b800:wherey*160-160 + wherex*2-2+1] := mem[$b800:wherey*160-160+wherex*2-2+1] and 7;
    mem[$b800:wherey*160-160 + wherex*2+1] := mem[$b800:wherey*160-160+wherex*2+1] and 7;
    gotoxy(curx + 2, ey + 1);
    for tel2 := 1 to tel do
      mem[$b800:(curx + 1 + tel2 - 1)*2 + (ey+1-1)*160 + 1] := mem[$b800:(curx + 1 + tel2 - 1)*2 + (wherey-1)*160 + 1] and 7;
    textcolor(white); textbackground(blue);


    if tel < ex - sx + 1 then begin
      inc(tel, popspeed);
      if tel > ex - sx + 1 then tel := ex - sx + 1;
    end else einde := true;
  end;
end;


procedure saveScreen;
var
  tel: integer;
begin
  for tel := 0 to 80*25*2-1 do screenbuf[tel+1] := mem[$b800:tel];
end;


procedure restoreScreen;
var
  tel: integer;
begin
  for tel := 0 to 80*25*2-1 do mem[$b800:tel] := screenbuf[tel+1];
end;


procedure removeBar(nr: integer);
begin
  textcolor(itemscolor); textbackground(blue);
  writeAt(29, optystart + nr - 1, optstr[nr]);
end;


procedure putBar(nr: integer);
begin
  textcolor(white+128); textbackground(lightblue);
  writeAt(29, optystart + nr - 1, optstr[nr]);
end;


procedure shutDown;
begin
  textmode(co80);
  textcolor(lightgray);
  textbackground(black);
end;


procedure viewTextFile(name: string);
var
  f: text;
  regelnr, tel: integer;
  ch: char;
  regel: string;
begin
  saveScreen;
  popupWindow(4, 2, 79, 23);

  writeAt(1, 25, '          ['#24'/'#25'] Scroll     [PgUp/PgDn/Home/End] Move     [Escape] Exit            ');
  textcolor(lightgreen);
  writeAt(11, 25, '['#24'/'#25']');
  writeAt(28, 25, '[PgUp/PgDn/Home/End]');
  writeAt(58, 25, '[Escape]');
  textcolor(white);

  cursorOn;
  gotoxy(32, 12);
  write('Reading file...');
  {$I-}
  assign(f, name);
  reset(f);
  if ioresult <> 0 then Fatal('Docfile not found');

  regelnr := 1;
  while not eof(f) do begin
    readln(f, regel);
{    if not eof(f) then begin}
     begin
      if regelnr > 20 then Fatal('2h');
      viewbuf[regelnr] := copy(regel, 1, 72);
      if ioresult <> 0 then Fatal('Error reading docfile');
      inc(regelnr);
    end;
  end;
  close(f);
  {$I+}

  cursorOff;
  writeAt(32, 12, '                   ');
  for tel := 1 to regelnr - 1 do
    writeAt(5, tel + 2, viewbuf[tel]);

  repeat; ch := readkey; until ch = #27;
  restoreScreen;
end;


procedure chooseOption(nr: integer);
var
  prog: string;
  wait: boolean;
begin
  if nr = aantoptions then begin
    shutDown;
    writeln('Twilight Easy Installation terminated.');
    halt(0);
  end;

  wait := true;
  prog := upcasestr(configstr[nr]);
  if copy(prog, 1, 8) = '$NOWAIT ' then begin
    wait := false;
    prog := copy(prog, 9, 255);
  end;
  if copy(prog, 1, 6) = '$VIEW ' then begin
    viewTextFile(copy(prog, 7, 255));
  end else begin
    saveScreen;
    textmode(co80);
    Exec (GetEnv ('COMSPEC'), '/C ' + prog);
    cursoroff;
    if wait then pause;
    disableBlinking;
    restoreScreen;
  end;
end;


procedure Main;
var
  balkpos: integer;
  einde: boolean;
begin
  einde := false;
  balkpos := 1;
  putBar(1);
  while not einde do begin
    ch := readkey;
    case ch of
      #13: chooseOption(balkpos);
      #27: einde := true;
      #0 : begin
        ch := readkey;
        case ch of
          #72 : if balkpos > 1 then begin
                  removeBar(balkpos);
                  dec(balkpos);
                  putBar(balkpos);
                end;
          #80: if balkpos < aantoptions then begin
                 removeBar(balkpos);
                 inc(balkpos);
                 putBar(balkpos);
               end;
          #71: if balkpos > 1 then begin
                 removeBar(balkpos);
                 balkpos := 1;
                 putBar(balkpos);
               end;
          #79: if balkpos < aantoptions then begin
                 removeBar(balkpos);
                 balkpos := aantoptions;
                 putBar(balkpos);
               end;
        end;
      end;
    end;
  end;
end;


function readConfigLine(var f: file): string;
var
  tel: integer;
  strsize: byte;
  dummystr: string;
begin
  {$I-}
  blockread(f, strsize, 1);
  blockread(f, dummystr[1], strsize);
  tel := ioresult;
  {$I+}
  if tel <> 0 then fatal('Unable to read config.');

  dummystr[0] := chr(strsize);

  for tel := 1 to length(dummystr) do begin
    dummystr[tel] := chr(ord(dummystr[tel]) xor ord(xorstr[xorcount]));
    inc(xorcount);
    if xorcount > length(xorstr) then xorcount := 1;
  end;

  readConfigLine := dummystr;
end;


procedure readConfig;
var
  tel, nr: integer;
  f: file;
  cfgoffset: longint;
  einde: boolean;
  regel3, regel2, cfgline: string;
  strsize: byte;
begin
  runstr := 'None'; setupstr := runstr; docstr := runstr; xorcount := 1;

  {$I-}
  assign(f, 'tl.exe');
  reset(f, 1);
  seek(f, filesize(f) - 4);
  blockread(f, cfgoffset, 4);
  seek(f, cfgoffset);
  blockread(f, strsize, 1);
  blockread(f, xorstr[1], strsize);
  tel := ioresult;
  {$I+}
  if tel <> 0 then Fatal('Unable to read config.');

  xorstr[0] := chr(strsize);
  for tel := 1 to length(xorstr) do xorstr[tel] := chr(ord(xorstr[tel]) xor 245);
  einde := false;
  while not einde do begin
    cfgline := readConfigLine(f);
    if upcasestr(cfgline) = 'END' then einde := true else
     if(cfgline[1] <> ';') and (cfgline <> '') and (pos('=', cfgline) > 0) then begin
      regel2 := upcasestr(copy(cfgline, 1, pos('=', cfgline) - 1));
      regel3 := copy(cfgline, pos('=', cfgline) + 1, 255);
      if regel2 = 'RUN' then runstr := regel3 else
       if regel2 = 'SETUP' then setupstr := regel3 else
       if regel2 = 'DOCS' then docstr := regel3 else
       fatal('Bad config.');
    end;
  end;

  nr := 0;
  if upcasestr(runstr) <> 'NONE' then begin
    inc(nr);
    optstr[1] := '       Run game       ';
    configstr[nr] := runstr;
  end;
  if upcasestr(setupstr) <> 'NONE' then begin
    inc(nr);
    optstr[nr] := '      Setup game      ';
    configstr[nr] := setupstr;
  end;
  if upcasestr(docstr) <> 'NONE' then begin
    inc(nr);
    optstr[nr] := '  Read documentation  ';
    configstr[nr] := docstr;
  end;
  inc(nr);
  optstr[nr] := '         Quit         ';

  aantoptions := nr;
end;


begin
  checkbreak := false;
  readConfig;
  textmode(co80);
  textbackground(blue);
  textcolor(white);

  clrscr;
  cursoroff;
  disableBlinking;

  tel2 := 0;
  for tel := 1 to 25*80 do begin
    mem [$b800:tel2] := 177;
    mem [$b800:tel2+1] := textattr;
    inc(tel2, 2);
  end;

  textcolor(white);
  textbackground(blue);
  writeAt(1, 25, '            ['#24'/'#25'] Select option     [Enter] Select     [Escape] Quit            ');
  textcolor(lightgreen);
  writeAt(13, 25, '['#24'/'#25']');
  writeAt(37, 25, '[Enter]');
  writeAt(56, 25, '[Escape]');

  textcolor(white);
  textbackground(blue);
  popupWindow(5, 3, 78, 12);

  writeAt(5, 4, '             ��������             ���              ��                   ');
  writeAt(5, 5, '            ��  ��  ��             ��              ��     ��            ');
  writeAt(5, 6, '                ��            ��   ��   ��         ��     ����� �� �  � ');
  writeAt(5, 7, '               ���   ��   �� ���   ��  ���  ������ ������ ��            ');
  writeAt(5, 8, '               ���   ���� �� ���  ���  ���  ��  �� �� ��� ���           ');
  writeAt(5, 9, ' �   �  �� ��������  ������� ���� ���� ���� ������ �� ��� �����         ');
  writeAt(5, 10, '                                               ���                      ');
  writeAt(5, 11, '                                             ����                       ');

  popupWindow(15, 15, 65, 18 + aantoptions);
  textcolor(yellow);
  writeAt(27, 16, 'Twilight Easy Installation');
  textcolor(white);

  textcolor(itemscolor);
  for tel := 1 to aantoptions do writeAt(29, 17 + tel, optstr[tel]);
  optystart := 18;

  main;
  shutDown;
  writeln('Twilight Easy Installation terminated.');
end.
