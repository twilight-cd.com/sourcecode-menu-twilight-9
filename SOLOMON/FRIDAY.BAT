Rem This batch file shows you how to make one thing happen on
Rem Fridays, and something else on others days of the week.
Rem you can use if for controlling the running of Toolkit programs
TkUtil weekday
IF ERRORLEVEL = 6 GOTO notfriday
IF ERRORLEVEL = 5 GOTO friday
IF ERRORLEVEL = 4 GOTO notfriday
IF ERRORLEVEL = 3 GOTO notfriday
IF ERRORLEVEL = 2 GOTO notfriday
IF ERRORLEVEL = 1 GOTO notfriday
IF ERRORLEVEL = 0 GOTO notfriday
:notfriday
Rem Today is not friday. Run the non-friday programs now.
Rem Non-Friday programs, if any, could go here.
Rem Remember that if you call another batch file, use "CALL BATCHFILE"
GOTO EXIT1
:friday
Rem Today is friday. Run the Friday programs now; for example:
Findviru c:
Rem And here you can test the Errorlevel, as in the FV.BAT file.
:EXIT1
