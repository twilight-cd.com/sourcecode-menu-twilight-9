        fprintf(g, "\n");
        fprintf(g, "               ��������             ���              ��\n");
        fprintf(g, "              ��  ��  ��             ��              ��     ��\n");
        fprintf(g, "                  ��            ��   ��   ��         ��     ����� �� �  �   �\n");
        fprintf(g, "                  ��   ��   �� ���   ��  ���  ������ ������ ��\n");
        fprintf(g, "                  ��   ���� �� ���  ���  ���  ��  �� �� ��� ���\n");
        fprintf(g, "   �   �  �� ��������  ������� ���� ���� ���� ������ �� ��� �����\n");
        fprintf(g, "                                                  ��\n");
        fprintf(g, "                                               ����\n");
        fprintf(g, "                                   Release %c\n", release + '0');

This is the source code for Twilight Menu 9. Twilights 1 to 9 contain a menu based on this source code.
It's written in Borland Turbo C, assembler and some pascal. It contains several artifacts of earlier releases and
several helper tools such as THF, TLDIAGS, TLMAKE, TLSETUP and HELPAPP. Twilight 9 shipped around January/February 1997.

Dig around, there is a lot of nice things in this project: at lease one xor key, funny and foul path names,
dutch/english mixed programming, stupidJoke() in MENU.C, easter eggs, policy, ideas, todos, secret passwords and more.


# Thanks to the authors

Thanks the the original authors for digging up this code and sharing it. As mentioned in the code and in the binaries of
later releases: KipSateHater, FatMan and Shroomz. :)

The source and all requirements are "as is", except for this readme file.


# Copyrighted content

This release contains some copyrighted materials. These are released together with the source of Twilight Menu
to be able to make it easier to build and understand this software.

If you have an issue with this, please create an issue or get in touch with one of the people who runs this project.
Use the contact form here: http://twilight-cd.com/contact/


# Compiling

If you get it to compile, please perform a pull request with an update of this file or raise an issue.


# Binary downloads

The binary release of Twilight 9 can be obtained from here: https://gitlab.com/twilight-cd.com/menu-binaries/-/tree/master/TWILIGHT009
You'll be able to run this in dosbox.

All menu binaries are released here: https://gitlab.com/twilight-cd.com/menu-binaries


# Background

Twilight was a physical collection of illegal software and games. Over a 100 releases on CD and DVD where made, having
a significant cultural impact in the Netherlands. It was a household name until the rise of the internet.

More history here: https://twilight-cd.com


# Release dates earlier twilights

Because of the dates of the first 10 releases where scrambled, it was impossible to know when the first twilight came
out. But alas, now we know.

      Release   Date in ini     Date on disc
    - TL01.INI: Unknown
    - TL02.INI: 12-03-1996
    - TL03.INI: 28-04-1996
    - TL04.INI: 05-06-1996
    - TL05.INI: 15-08-1996
    - TL06.INI: 26-08-1996
    - TL07.INI: 30-10-1996      08-11-1996
    - TL08.INI: 29-11-1996
    - TL09.INI: Unknown
