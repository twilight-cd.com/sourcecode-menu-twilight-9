#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <string.h>
#include <process.h>
#include <stdlib.h>

#include "tl.h"

//#include <dos.h>
//#include <string.h>



/*
 * getVolumeLabel
 *
 * Function: Retrieves the volume label of a specified drive.
 *
 * Parameters: drive = drive-character (e.g. 'C')
 *             *dest = pointer to buffer to put label in
 */

int getVolumeLabel(char drive, unsigned char *dest)
{
	int errcode;
	char *path = "::\*.*";
	char *src;
	struct find_t ffblk;

	path[0] = drive;
	if((errcode = _dos_findfirst(path, FA_LABEL, &ffblk)) != 0) return errcode;

	src = ffblk.name;
	do {
		if(*src != '.') *dest++ = *src;
	} while(*src++);

	return 0;
}
